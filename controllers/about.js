const express = require('express')
const api = express.Router()

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------

// GET t1
api.get('/t1', (req, res) => {
 console.log(`Handling GET ${req}`)
 res.render('about/t1/index.ejs',
       { title: 'TeamName', layout: 'layout.ejs' })
})
api.get('/t1/a', (req, res) => {
 console.log(`Handling GET ${req}`)
 res.render('about/t1/a/index.ejs',
       { title: 'David Graf', layout: 'layout.ejs' })
})
api.get('/t1/b', (req, res) => {
 console.log(`Handling GET ${req}`)
 return res.render('about/t1/b/index.ejs',
       { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
})
// GET t2... through t12 here...
// GET t2
api.get('/t2', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t2/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
    })
    api.get('/t2/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t2/a/index.ejs',
            { title: 'Garrett Scribner', layout: 'layout.ejs' })
    })
    api.get('/t2/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t2/b/index.ejs',
            { title: 'Sam Gedwel', layout: 'layout.ejs' })
    })
    
    // GET t3
    api.get('/t3', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t3/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
    })
    api.get('/t3/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t3/a/index.ejs',
            { title: 'Aawaj Raj Joshi', layout: 'layout.ejs' })
    })
    api.get('/t3/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t3/b/index.ejs',
            { title: 'Sreevani Anoohya Tadiboina', layout: 'layout.ejs' })
    })
    
    // GET t4
    api.get('/t4', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t4/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
    })
    api.get('/t4/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t4/a/index.ejs',
            { title: 'Nista Shrestha', layout: 'layout.ejs' })
    })
    api.get('/t4/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t4/b/index.ejs',
            { title: 'Rahul Reddy Lankala', layout: 'layout.ejs' })
    })
    api.get('/t4/c', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t4/c/index.ejs',
            { title: 'Lhakpa Sherpa', layout: 'layout.ejs' })
    })
    
    // GET t5
    api.get('/t5', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t5/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
    })
    api.get('/t5/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t5/a/index.ejs',
            { title: 'Jacob Katsion', layout: 'layout.ejs' })
    })
    api.get('/t5/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t5/b/index.ejs',
            { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
    })
    api.get('/t5/c', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t5/c/index.ejs',
            { title: 'Jacob Katsion', layout: 'layout.ejs' })
    })
    
    // GET t6
    api.get('/t6', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t6/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
    })
    api.get('/t6/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t6/a/index.ejs',
            { title: 'Gunhee Lee', layout: 'layout.ejs' })
    })
    api.get('/t6/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t6/b/index.ejs',
            { title: 'Christoper Moody', layout: 'layout.ejs' })
    })
    api.get('/t6/c', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t6/c/index.ejs',
            { title: 'Pemba Sherpa', layout: 'layout.ejs' })
    })
    // GET t7
    api.get('/t7', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t7/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
    })
    api.get('/t7/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t7/a/index.ejs',
            { title: 'TeamMember1PutYourNameHere', layout: 'layout.ejs' })
    })
    api.get('/t7/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t7/b/index.ejs',
            { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
    })
    
    // GET t8
    api.get('/t8', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t8/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
    })
    api.get('/t8/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t8/a/index.ejs',
            { title: 'TeamMember1PutYourNameHere', layout: 'layout.ejs' })
    })
    api.get('/t8/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t8/b/index.ejs',
            { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
    })
    
    // GET t9
    api.get('/t9', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t9/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
    })
    api.get('/t9/c', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t9/c/index.ejs',
            { title: 'Lhakpa Sherpa', layout: 'layout.ejs' })
    })
//     api.get('/t9/a', (req, res) => {
//       console.log(`Handling GET ${req}`)
//       res.render('about/t9/a/index.ejs',
//             { title: 'TeamMember1PutYourNameHere', layout: 'layout.ejs' })
//     })
    api.get('/t9/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t9/b/index.ejs',
            { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
    })
    
    // GET t10
    api.get('/t10', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t10/index.ejs',
            { title: 'Unit 10', layout: 'layout.ejs' })
    })
    api.get('/t10/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t10/a/index.ejs',
            { title: 'David M. Small', layout: 'layout.ejs' })
    })
    // GET t11
    api.get('/t11', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t11/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
    })
    api.get('/t11/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t11/a/index.ejs',
            { title: 'TeamMember1PutYourNameHere', layout: 'layout.ejs' })
    })
    api.get('/t11/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t11/b/index.ejs',
            { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
    })
    // GET t12
    api.get('/t12', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t12/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
    })
    api.get('/t12/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t12/a/index.ejs',
            { title: 'TeamMember1PutYourNameHere', layout: 'layout.ejs' })
    })
    api.get('/t12/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t12/b/index.ejs',
            { title: 'TeamMember2PutYourNameHere', layout: 'layout.ejs' })
    })
     
//and then finally:

module.exports = api
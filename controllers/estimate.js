
const express = require('express')
const api = express.Router()
const Model = require('../models/estimate.js')
const LOG = require('../utils/logger.js') // comment out until exists
const find = require('lodash.find')
const remove = require('lodash.remove')
const notfoundstring = 'estimates'
const passport = require('../config/passportConfig.js')

//Details-----api.get('/details/:id'---unit 1
api.get('/details/:id',  (req, res) => {
  LOG.info(`Handling GET /details/:id ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.estimates.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('estimate/details.ejs',
    {
      title: 'estimate Details',
      layout: 'layout.ejs',
      estimate: item
    })
})

// Index------api.get('/'1 -------------unit 1
api.get('/', (req, res) => {
  res.render('estimate/index.ejs')
})


// findall ---api.get('/findall'--------unit 2
api.get('/findall', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const data = req.app.locals.estimates.query
  res.send(JSON.stringify(data))
})

// findone ---api.get('/findone/:id' ---unit 3
api.get('/findone/:id', (req, res) => {
res.setHeader('Content-Type', 'application/json')
const id = parseInt(req.params.id, 10) 
const data = req.app.locals.estimates.query
const item = find(data, { _id: id }) 
res.send(JSON.stringify(item))
}) 

//Create------api.get('/create'---------unit 4
api.get('/create', (req, res) => {
  LOG.info(`Handling GET /create${req}`)
  const item = new Model()
  LOG.debug(JSON.stringify(item))
  res.render('estimate/create.ejs',
    {
      title: 'Create Estimates',
      layout: 'layout.ejs',
      estimate: item
    })
})

//delete     api.post('/delete/:id'-----unit 5
api.post('/delete/:id', (req, res) => {
    LOG.info(`Handling DELETE request ${req}`)
    const id = parseInt(req.params.id, 10) // base 10
    LOG.info(`Handling REMOVING ID=${id}`)
    const data = req.app.locals.estimates.query
    const item = find(data, { _id: id })
    if (!item) {
      return res.end(notfoundstring)
    }
    if (item.isActive) {
      item.isActive = false
      console.log(`Deacctivated item ${JSON.stringify(item)}`)
    } else {
      const item = remove(data, { _id: id })
      console.log(`Permanently deleted item ${JSON.stringify(item)}`)
    }
    return res.redirect('/estimate')
  })

//select      api.get('/select'---------unit 6
api.get('/select', (req, res) => {
  LOG.info(`Handling GET /select${req}`)
  const item = new Model()
  LOG.debug(JSON.stringify(item))
  res.render('estimate/select',
    {
      title: 'Select Estimate',
      layout: 'layout.ejs',
      estimate: item
    })
})
//Delete------api.get('/delete/:id'-----unit 7
api.get('/delete/:id', (req, res) => {
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.estimates.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('estimate/delete.ejs',
    {
      title: 'Delete entries',
      layout: 'layout.ejs',
      estimate: item
    })
})

//Edit--------api.get('/edit/:id'-------unit 8

api.get('/edit/:id', (req, res) => {
  LOG.info(`Handling GET /edit/:id ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.estimates.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
  return res.render('estimate/edit.ejs',
    {
      title: 'estimates',
      layout: 'layout.ejs',
      estimate: item
    })
})

//copyfrom    api.post('/copyfrom/:id---unit 9

//print       api.get('/print/:id'------unit10

//update       api.post('/save/:id'-----unit11
api.post('/save/:id', (req, res) => {
  LOG.info(`Handling SAVE request ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  LOG.info(`Handling SAVING ID=${id}`)
  const data = req.app.locals.estimates.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
  item.name = req.body.name
  item.location = req.body.location
  item.squareFeet = parseInt(req.body.squareFeet, 10)
  item.materials = []
  //item.materials.length = 4
  item.materials.length = 0
  if (req.body.product.length > 0) {
    //commit
    for (let count = 0; count < req.body.product.length; count++) {
      item.materials.push(
        {
          product: req.body.product[count],
          unitcost: req.body.unitcost[count],
          coverageSquareFeetPerUnit: parseInt(req.body.coverageSquareFeetPerUnit[count], 10)
        }
      )
    }
    LOG.info(`SAVING UPDATED estimate ${JSON.stringify(item)}`)
    return res.redirect('/estimate')
  }
})

//insert new   api.post('/save'  -------unit12
api.post('/save', (req, res) => {
  LOG.info(`Handling POST ${req}`)
  LOG.debug(JSON.stringify(req.body))
  const data = req.app.locals.estimates.query
  const item = new Model()
  LOG.info(`NEW ID ${req.body._id}`)
  item._id = parseInt(req.body._id, 10) // base 10
  item.name = req.body.name
  item.location = req.body.location
  item.squareFeet = parseInt(req.body.squareFeet, 10)
  item.materials = []
  item.materials.length = 0
  if (req.body.materialName.length > 0) {
    for (let count = 0; count < req.body.materialName.length; count++) {
      item.materials.push(
        {
          name: req.body.materialName[count],
          unitcost: parseInt(req.body.unitCost[count], 10),
          coverageSquareFeetPerUnit: parseInt(req.body.squareFeetPerUnit[count], 10)
        }
      )
    }
    data.push(item)
    LOG.info(`SAVING NEW estimate ${JSON.stringify(item)}`)
    return res.redirect('/estimate')
  }
})

module.exports = api


/**

 *

 *  +----------------------------------------------------------------------------------------------------------------+

 *      |          Copyright - Northest Missouri State University                 | 

*       |-----------------------------------------------------------------------------------------------------------------|

 *      |                             Team # 4                                                                                                        | 

 *      |              Unit # 4: Nista Shrestha, Rahul lankala                    | 

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |                       Description: test.js                             | 
    +---------------------------------------------------------------------------------------------------------------------|
 * */


var expect = require('chai').expect;

var Estimate = require('../../models/estimate');

var assert= require("assert")

describe('Estimate', function() {
    it('should be invalid if name is empty', function(done) {
        var m = new Estimate();
        m.validate(function(err) {
            expect(m.name).to.be.a('String');
            done();
        });
    });

    
    it('should be invalid if id is empty', function(done) {
		var e = new Estimate();

		e.validate(function(err) {
			expect(err.errors._id).to.exist;
			done();
		});
    });
    it('should be invalid if squareFeet is String', function(done) {
        var m = new Estimate();
        m.validate(function(err) {
            expect(m.squareFeet).to.be.a('Number');
            done();
        });
    });
    it ('should be invalid if number of people is String'), function(done){
        var k= new Estimate();
        k.validate(function(err){
           expect(k.numberOfPeople).to.be.a('Number');
           done();
        })
    }
});







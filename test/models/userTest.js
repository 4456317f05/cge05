var expect = require('chai').expect;

var User = require('../../models/user');

describe('User', function() {
    it('should be valid if email is empty', function(done) {
        var u = new User();        
        u.validate(function(err) {
            expect(u.email).to.exist;
            done();
        });
    });
});

describe('User', function() {
	it('should be invalid if password is empty', function(done) {
		var u = new User();
		u.validate(function(err) {
			expect(u.password).to.exist;
			done();
		});
	});
});

describe('User', function() {
	it('should be invalid if password is empty', function(done) {
		var u = new User();
		u.validate(function(err) {
			expect(u.password).to.exist;
			done();
		});
	});
});

describe('estimate', function() {
    it('should be invalid if email is String', function(done) {
        var m = new User();
 
        m.validate(function(err) {
            expect(m.email).to.be.a('String');
            done();
        });
    });
});

describe('estimate', function() {
    it('should be invalid if email is NUll', function(done) {
        var m = new User();
 
        m.validate(function(err) {
            expect(m.email).not.to.be.null;
            done();
        });
    });
});
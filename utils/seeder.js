/* ==========================================================================
   needed for BootStrap menu
   ========================================================================== */
   /**

 *

 *      +----------------------------------------------------------------------------------------------------------------+

 *      |                                             Copyright - Northest Missouri State University                  | 

*       |-----------------------------------------------------------------------------------------------------------------|

 *      |Team # 4                                                                                                     | 

 *      |Unit # 9 : Lhakpa Sonam Sherpa                                                  | 

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |Description: Custom logger page                                                                   | 

 * */

// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')
const userController = require('../controllers/user')
const users = require('../data/users.json')
const estimates = require('../data/estimates.json')

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = new Datastore()
  db.loadDatabase()

  // insert the sample data into our data store
  db.insert(estimates)
  app.locals.users = JSON.parse(JSON.stringify(users))
  // initialize app.locals (these objects will be available to our controllers)
  app.locals.estimates = db.find(estimates)

  LOG.debug(`${estimates.length} estimates`)
  LOG.info('END Seeder. Sample data read and verified.')
}

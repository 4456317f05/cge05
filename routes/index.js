/**
* @index.js - manages all routing
*
* router.get when assigning to a single request
* router.use when deferring to a controller
*
* @requires express
*/

const express = require('express')
const LOG = require('../utils/logger.js')  // comment out until exists
LOG.debug('START routing')   // comment out until exists
const router = express.Router()

// Manage top-level request first
router.get('/welcome', (req, res, next) => {
 LOG.debug('Request to /')  // comment out until exists
 res.render('index.ejs', { title: 'CGE05' }) //  use your sec num
})

// Defer path requests to a particular controller
router.use('/about', require('../controllers/about.js'))
router.use('/estimate', require('../controllers/estimate.js'))
router.use('/', require('../controllers/estimate.js'))


LOG.debug('END routing')
module.exports = router
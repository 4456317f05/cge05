const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

 email: { type: String, unique: true },
 password: String,
 passwordResetToken: String,
 passwordResetExpires: Date,
 tokens: Array,
 profile: {
   name: String
 }
}, 
{ timestamps: true })

module.exports = mongoose.model('User', userSchema)

/* Copyright - Northest Missouri State University
Team 2 Sam Gedwillo, Garrett Scribner
Description: This is for about.ejs
NAME                            VERSION                       CHANGES
Name                          0.0.1(Initial)                      change log
*/